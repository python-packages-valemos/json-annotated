from enum import Enum

from .i_json_serializable import IJsonSerializable


class EnumByNameJson(IJsonSerializable, Enum):
    """Deserialize string from json as if it equals enum key"""

    def to_json(self):
        return self.name

    @classmethod
    def from_json(cls, json_object: str, skip_not_existing: bool):
        return cls[json_object]


class EnumByValueJson(IJsonSerializable, Enum):
    """
    Deserialize string from json as if it equals enum string value
    For custom object type to be supported use EnumMappingJson
    """

    def to_json(self):
        return self.value

    @classmethod
    def from_json(cls, json_object, skip_not_existing: bool):
        return cls(json_object)
