
from .i_json_serializable import IJsonSerializable
from .metaclasses import CompositeJsonScheme


class ACompositeJson(IJsonSerializable, metaclass=CompositeJsonScheme):
    """
    Only annotated attributes will be serialized
    Object must have all fields be initialized by default
    """

    def to_json(self):
        result = {}
        for attr in self.__class__.__serialized__:
            value = getattr(self, attr)
            if attr in self.__serializable_children__:
                result[attr] = IJsonSerializable._object_to_json(value)
            elif attr in self.__serializable_arrays__:
                result[attr] = [IJsonSerializable._object_to_json(element) for element in value]
            else:
                raise ValueError(f'cannot serialize "{attr}"')
        return result

    @classmethod
    def from_json(cls, json_object: dict, skip_not_existing=False):
        obj = cls()
        for attr in cls.__serialized__:
            if attr not in json_object:
                if skip_not_existing: continue
                raise ValueError(f'attribute {attr} was not provided in json')
            
            cls._deserialize_attribute(obj, attr, json_object, skip_not_existing)
        return obj

    @classmethod
    def _deserialize_attribute(cls, obj, attr, json_object, skip_not_existing):
        json_child = json_object[attr]
        if attr in cls.__serializable_children__:
            element_type = cls.__serializable_children__[attr]
            setattr(obj, attr, cls._object_from_json(element_type, json_child, skip_not_existing))

        elif attr in cls.__serializable_arrays__:
            array_type, element_type = cls.__serializable_arrays__[attr]
            setattr(obj, attr, array_type(cls._object_from_json(element_type, el, skip_not_existing) for el in json_child))
        else:
            raise ValueError('cannot deserialize, schema changed!')
