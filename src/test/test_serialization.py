import dataclasses
import unittest
import json

from src.json_annotated.a_composite_json import ACompositeJson
from src.json_annotated.raw_json import RawJson
from src.json_annotated.enum_json import EnumByNameJson, EnumByValueJson
from src.json_annotated.enum_named_objects import EnumNamedObjects
from src.json_annotated.i_json_serializable import IJsonSerializable
from src.json_annotated.named_object import NamedObject


class EnumTest(EnumByNameJson):
    VALUE1 = "4"
    VALUE2 = "10"


class EnumValueTest(EnumByValueJson):
    VALUE1 = "4"
    VALUE2 = "10"


@dataclasses.dataclass
class CompositeEnumTest(ACompositeJson):
    value: int = 0
    en: EnumTest = EnumTest.VALUE1
    ls: list[EnumTest] = dataclasses.field(default_factory=list)
    opt: str = None

    def __eq__(self, other):
        return self.value == other.value and \
                self.en == other.en and \
                self.ls == other.ls and \
                self.opt == other.opt


@dataclasses.dataclass
class OptionalWrapper(ACompositeJson):
    internal: CompositeEnumTest = None


class TestSerializationDeserialization(unittest.TestCase):

    def setUp(self) -> None:
        self.composite_with_enum = CompositeEnumTest(10, EnumTest.VALUE2, [EnumTest.VALUE2, EnumTest.VALUE1], "string")
        self.arbitrary_json = RawJson([5, "str", {"dict": 10}])

    def test_composite(self):
        self.obj_test(self.composite_with_enum)

    def test_deserialize_optional(self):
        optional_dict = self.composite_with_enum.to_json()
        del optional_dict["opt"]
        
        loaded_optional = self.composite_with_enum.__class__.from_json(
            optional_dict, skip_not_existing=True)
        
        default_optional = self.composite_with_enum.__class__()
        self.assertEqual(default_optional.opt, loaded_optional.opt)

        self.composite_with_enum.opt = default_optional.opt
        self.assertEqual(self.composite_with_enum, loaded_optional)

    def test_deserialize_internal_optional(self):
        opt_wrapper = OptionalWrapper(self.composite_with_enum)
        optional_dict = opt_wrapper.to_json()
        
        del optional_dict["internal"]["opt"]
        
        loaded_wrapper = OptionalWrapper.from_json(
            optional_dict, skip_not_existing=True)
        
        default_optional = self.composite_with_enum.__class__()
        self.assertEqual(default_optional.opt, loaded_wrapper.internal.opt)

        opt_wrapper.internal.opt = default_optional.opt
        self.assertEqual(opt_wrapper, loaded_wrapper)

    def test_arbitrary(self):
        self.obj_test(self.arbitrary_json)

    def test_named_object_enum(self):
        class Tester(EnumNamedObjects):
            S1 = NamedObject("item", 1000)

        self.assertFalse(Tester.has_name("boi"))
        self.assertTrue(Tester.has_name("item"))
        self.assertEqual(Tester.S1.get_object(), 1000)
        self.assertEqual(Tester("item").get_object(), 1000)
        
    def obj_test(self, obj: IJsonSerializable):
        j = obj.to_json()
        obj2 = obj.__class__.from_json(j)
        self.assertEqual(obj, obj2)
        self.assertEqual(obj.__class__.from_json(json.loads(json.dumps(obj.to_json()))), obj)
